#!/bin/bash
#path to https://bitbucket.org/crv4all/build-scripts
PATH_TO_BUILD_SCRIPTS=~/bovaris/build-scripts/scripts
#path to the dv folders for kitchen dv box
PATH_TO_DV_FOLDER=~/bovaris/dvs

cd $PATH_TO_BUILD_SCRIPTS
python vdbDeploy.py -h 192.168.50.152 -v herdbook.vdb -f $PATH_TO_DV_FOLDER/dv-herdbook/herdbook-vdb.vdb -u admin -p admin -s local -am basic -mp 9992
python vdbDeploy.py -h 192.168.50.152 -v breeding.vdb -f $PATH_TO_DV_FOLDER/dv-breeding/breeding-vdb.vdb -u admin -p admin -s local -am basic -mp 9992
python vdbDeploy.py -h 192.168.50.152 -v customer.vdb -f $PATH_TO_DV_FOLDER/dv-customer/customer-vdb.vdb -u admin -p admin -s local -am basic -mp 9992
python vdbDeploy.py -h 192.168.50.152 -v fertility.vdb -f $PATH_TO_DV_FOLDER/dv-fertility/fertility-vdb.vdb -u admin -p admin -s local -am basic -mp 9992
