#!/bin/bash
PATH_TO_DEVBOX=~/bovaris/basecamp-jbosseap-vagrant
PATH_TO_YOUR_WORKSPACE=~/bovaris/workspace

cd $PATH_TO_DEVBOX
STATUS="`vagrant status | grep 'running'`"
case $STATUS in
	*dev.bovaris.io*running*)
		echo "dev.bovaris is already running so we do not need to bring it up"
		;;
	*)
		vagrant up dev.bovaris.io
		;;
esac

cd $PATH_TO_YOUR_WORKSPACE
for folder in *; do
	if [[ -d "$folder" && ! -L "$folder" ]]; then
		if [[ $folder != *"end2end-test"* ]]; then #don't need to deploy end2end-tests so if the  current folder is one we skip that folder
			if [[ $folder == *"domain"* || $folder == *"adapter"* || $folder == *"backend"* || $folder == *"catalog"* || $folder == *"pump"* ]]; then
				cd $folder
				echo "======================================"
				echo "=======now in $folder====="
				echo "======================================"
				git checkout develop
				git pull
				sudo chmod +x ./gradlew
				./gradlew clean deploy
				cd ..
	   		fi
		fi
   	fi
done
